import apiFetch from '../../core/fetch/apiFetch';

const _baseEndPoint = 'userroles';

export default class UserRole {
    static async search() {
        return apiFetch(_baseEndPoint);
    }
}