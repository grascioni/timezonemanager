import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Form } from 'reactstrap';
import User from './user';
import UserRole from './userRole';
import UserForm from './UserForm';
import makeValidable from '../../core/components/Validable';
import Button from '../../core/components/Button';
import { NotificationManager } from 'react-notifications';

const ValidableUserForm = makeValidable(UserForm);

class EditUserPage extends Component {
    constructor(props) {
        super(props);
        this.validableRef = React.createRef();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.state = {
            user: null,
            saving: false,
            deleting: false,
            showValidations: false,
            loading: true,
            userRoles: []
        };
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({ showValidations: true }, async () => {
            if (this.validableRef.current.isValid() && this.validateForm()) {
                this.setState({ saving: true });
                this.state.user.update()
                    .then(() => {
                        NotificationManager.success(`User saved`, '', 3 * 1000);
                        this.props.history.push('/users');
                    })
                    .finally(() => this.setState({ saving: false }));

            }
        });
    }

    handleDelete() {
        this.setState({ deleting: true });
        this.state.user.delete()
            .then(() => {
                NotificationManager.success(`User deleted`, '', 3 * 1000);
                this.props.history.push('/users');
            })
            .finally(() => this.setState({ deleting: false }));
    }

    validateForm() {
        if (this.state.user.password !== this.state.user.confirmPassword) {
            NotificationManager.error('Passwords do not match!');
            return false;
        }
        return true;
    };


    async componentDidMount() {
        const id = this.props.match.params.id;
        const user = await User.getById(id);
        const userRoles = await UserRole.search();
        this.setState({ user, userRoles, loading: false });
    }

    render() {
        const { user, showValidations, saving, deleting, userRoles } = this.state;
        if (!user) { return 'Loading...' };
        return (
            <div className="container">
                <Form onSubmit={this.handleSubmit} noValidate={true}>
                    <ValidableUserForm
                        ref={this.validableRef}
                        showValidations={showValidations}
                        user={user}
                        userChanged={updated => this.setState({ user: new User(updated) })}
                        userRoles={userRoles} />
                    <div className="row">
                        <div className="col">
                            <Button
                                label="Save"
                                type="submit"
                                className="mt-2 ml-2 btn btn-primary"
                                showSpinner={saving}
                                onClick={this.handleSubmit}
                            />
                            <Button
                                label="Delete"
                                className="mt-2 ml-2 btn btn-danger"
                                showSpinner={deleting}
                                onClick={this.handleDelete}
                            />
                        </div>
                    </div>
                </Form>
            </div>
        );
    }
}

export default withRouter(EditUserPage);
