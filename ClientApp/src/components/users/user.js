import apiFetch from '../../core/fetch/apiFetch';

const _baseEndPoint = 'users';

export default class User {
    constructor(item) {
        if (item) {
            this.userId = item.userId;
            this.username = item.username;
            this.firstName = item.firstName;
            this.lastName = item.lastName;
            this.userRoleId = item.userRoleId;
            this.password = item.password;
            this.confirmPassword = item.confirmPassword;
            return;
        }
        this.userId = null;
        this.username = '';
        this.firstName = '';
        this.lastName = '';
        this.userRoleId = null;
        this.password = '';
        this.confirmPassword = '';
    }

    static async search() {
        return apiFetch(_baseEndPoint);
    }

    static async getById(id) {
        const entity = await apiFetch(`${_baseEndPoint}/${id}`);
        return new User(entity);
    }

    static create(item) {
        return apiFetch(_baseEndPoint, 'POST', item);
    }

    update() {
        return apiFetch(`${_baseEndPoint}/${this.userId}`, 'PUT', this);
    }

    delete() {
        return apiFetch(`${_baseEndPoint}/${this.userId}`, 'DELETE', this);
    }
}