import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import User from './user';

class UserListPage extends Component {
    state = {
        users: [],
        loading: true
    };

    componentDidMount() {
        User.search()
            .then(data => {
                this.setState({ users: data, loading: false });
            });
    }

    render() {
        return (
            <div className="container">
                <Link to={`/users/create`}>New User</Link>
                <hr />
                <ul>
                    {this.state.users.map((x, idx) => (
                        <li key={idx}>
                            <Link to={`/users/edit/${x.userId}`}>{x.fullName}</Link>
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}

export default withRouter(UserListPage);
