import FormDropdown from '../../core/components/FormDropdown';
import FormInput from '../../core/components/FormInput';
import PropTypes from 'prop-types';
import React, { Fragment } from 'react';
import User from './user';

export default class UserForm extends React.Component {
    constructor(props) {
        super(props);
        this.updateUser = this.updateUser.bind(this);
    }

    static propTypes = {
        user: PropTypes.instanceOf(User).isRequired,
        userChanged: PropTypes.func.isRequired,
        userRoles: PropTypes.array
    };

    updateUser(property, value) {
        const { userChanged, user } = this.props;
        userChanged({ ...user, [property]: value })
    }

    render() {
        const { user, userRoles } = this.props;
        return (
            <Fragment>
                <div className="row">
                    <div className="col">
                        <FormInput id="username" label="User Name" value={user.username} onChange={e => this.updateUser(e.target.id, e.target.value)} required />
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <FormInput id="firstName" label="First Name" value={user.firstName} onChange={e => this.updateUser(e.target.id, e.target.value)} required />
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <FormInput id="lastName" label="Last Name" value={user.lastName} onChange={e => this.updateUser(e.target.id, e.target.value)} required />
                    </div>
                </div>
                {userRoles &&
                    <div className="row">
                        <div className="col">
                            <FormDropdown
                                placeholder="Select a role..."
                                label="User Role"
                                items={userRoles.map(x => ({ id: x.userRoleId, label: x.description }))}
                                selectedId={user.userRoleId}
                                onChange={value => this.updateUser('userRoleId', value)}
                                required
                            />
                        </div>
                    </div>}
                <div className="row">
                    <div className="col">
                        <FormInput id="password" label="Password" type="password" value={user.password} onChange={e => this.updateUser(e.target.id, e.target.value)} required />
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <FormInput id="confirmPassword" label="Confirm Password" type="password" value={user.confirmPassword} onChange={e => this.updateUser(e.target.id, e.target.value)} required />
                    </div>
                </div>
            </Fragment>
        );
    }
}