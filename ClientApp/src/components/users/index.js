import React from 'react';
import { Route, Switch } from 'react-router-dom';
import UserList from './UserListPage';
import NewUserPage from './NewUserPage';
import EditUserPage from './EditUserPage';

export default () => (
    <Switch>
        <Route exact path="/users" component={UserList} />
        <Route exact path="/users/create" component={NewUserPage} />
        <Route exact path="/users/edit/:id?" component={EditUserPage} />
    </Switch>
);
