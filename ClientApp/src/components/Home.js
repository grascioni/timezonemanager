import React, { Component } from 'react';

export class Home extends Component {
    static displayName = Home.name;

    render() {
        return (
            <div className="container">
                <h1>Hello, world!</h1>
                <p>Gustavo Rascioni take-home project</p>
            </div>
        );
    }
}
