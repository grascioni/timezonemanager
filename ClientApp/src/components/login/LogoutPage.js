import { clearLoginData } from '../../core/login/storage';

export default ({ history }) => {
    clearLoginData(null);
    history.push('/login');
    return '';
};
