import React, { Component } from 'react';
import { NotificationManager } from 'react-notifications';
import { withRouter } from 'react-router-dom';
import Form from './LoginForm';
import { login } from '../../core/fetch/auth';
import { saveLoginData } from '../../core/login/storage';
import makeValidable from '../../core/components/Validable';

const ValidableForm = makeValidable(Form);

class Login extends Component {
    constructor(props) {
        super(props);
        this.validableRef = React.createRef();
        this.state = {
            credentials: {
                userName: '',
                password: ''
            },
            loading: false,
            showValidations: false
        };
    }


    onSubmit = async () => {
        this.setState({ loading: true, showValidations: true }, async () => {
            if (this.validableRef.current.isValid()) {
                try {
                    const result = await login(this.state.credentials);
                    saveLoginData(result);
                    NotificationManager.success(`Welcome ${result.userData.name}`, '', 3 * 1000);
                    this.props.history.push('/');
                } catch (e) {
                    NotificationManager.error(e, '', 3 * 1000);
                }
            }
        });
        this.setState({ loading: false });
    };

    render() {
        return (
            <div className="mt-3">
                <ValidableForm
                    ref={this.validableRef}
                    showValidations={this.state.showValidations}
                    loading={this.state.loading}
                    credentials={this.state.credentials}
                    onSubmit={this.onSubmit}
                    onCredentialsUpdated={newCredentials =>
                        this.setState({
                            credentials: { ...newCredentials }
                        })
                    }
                />
            </div>
        );
    }
}

export default withRouter(Login);
