import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'reactstrap';
import CardWrapper, { wrapperWidth } from '../../core/components/CardWrapper';
import Button from '../../core/components/Button';
import FormInput from '../../core/components/FormInput';
import { Link } from 'react-router-dom';

const LoginForm = ({
    loading,
    credentials,
    onSubmit,
    onCredentialsUpdated
}) => {
    const updateCredentials = e => {
        onCredentialsUpdated({
            ...credentials,
            [e.target.id]: e.target.value
        });
    };
    const handleSubmit = e => {
        e.preventDefault();
        onSubmit();
    };

    return (
        <CardWrapper title="Time Zone Manager Login" width={wrapperWidth.s}>
            <Form onSubmit={handleSubmit} noValidate={true}>
                <FormInput
                    id="userName"
                    type="text"
                    label="User Name"
                    value={credentials.userName}
                    onChange={updateCredentials}
                    required
                />
                <FormInput
                    id="password"
                    type="password"
                    label="Password"
                    value={credentials.password}
                    onChange={updateCredentials}
                    required
                />
                <Link className="pull-left" to={`/register`}>Register</Link>
                <div className="mx-auto d-flex flex-row flex-wrap justify-content-end">
                    <Button
                        label="Login"
                        type="submit"
                        className="mt-2 ml-2 btn btn-primary"
                        showSpinner={loading}
                        onClick={handleSubmit}
                    />
                </div>
            </Form>
        </CardWrapper>
    );
};

LoginForm.propTypes = {
    loading: PropTypes.bool.isRequired,
    credentials: PropTypes.shape({
        userName: PropTypes.string.isRequired,
        password: PropTypes.string.isRequired
    }).isRequired,
    onSubmit: PropTypes.func.isRequired,
    onCredentialsUpdated: PropTypes.func.isRequired
};

export default LoginForm;
