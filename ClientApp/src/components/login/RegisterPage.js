import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Form } from 'reactstrap';
import User from '../users/user';
import UserForm from '../users/UserForm';
import makeValidable from '../../core/components/Validable';
import Button from '../../core/components/Button';
import { register } from '../../core/fetch/auth';
import { NotificationManager } from 'react-notifications';

const ValidableUserForm = makeValidable(UserForm);

class RegisterPage extends Component {
    constructor(props) {
        super(props);
        this.validableRef = React.createRef();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            user: new User(),
            saving: false,
            showValidations: false
        };
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({ showValidations: true }, async () => {
            if (this.validableRef.current.isValid() && this.validateForm()) {
                this.setState({ saving: true });
                register(this.state.user)
                    .then(() => {
                        NotificationManager.success(`User registered`, '', 3 * 1000);
                        this.props.history.push('/');
                    })
                    .catch(() => this.setState({ saving: false }));

            }
        });
    }

    validateForm() {
        if (this.state.user.password !== this.state.user.confirmPassword) {
            NotificationManager.error('Passwords do not match!');
            return false;
        }
        return true;
    };

    render() {
        const { user, showValidations, saving } = this.state;
        return (
            <div className="container">
                <Form onSubmit={this.handleSubmit} noValidate={true}>
                    <ValidableUserForm
                        ref={this.validableRef}
                        showValidations={showValidations}
                        user={user}
                        userChanged={updated => this.setState({ user: new User(updated) })}
                    />
                    <div className="row">
                        <div className="col">
                            <Button
                                label="Save"
                                type="submit"
                                className="mt-2 ml-2 btn btn-primary"
                                showSpinner={saving}
                                onClick={this.handleSubmit}
                            />
                        </div>
                    </div>
                </Form>
            </div>
        );
    }
}

export default withRouter(RegisterPage);
