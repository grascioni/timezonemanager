import FormInput from '../../core/components/FormInput';
import PropTypes from 'prop-types';
import React, { Fragment } from 'react';
import TimeZone from './timeZone';

export default class TimeZoneForm extends React.Component {
    constructor(props) {
        super(props);
        this.updateTimeZone = this.updateTimeZone.bind(this);
    }

    static propTypes = {
        timeZone: PropTypes.instanceOf(TimeZone).isRequired,
        timeZoneChanged: PropTypes.func.isRequired
    };

    updateTimeZone(property, value) {
        const { timeZoneChanged, timeZone } = this.props;
        timeZoneChanged({ ...timeZone, [property]: value })
    }

    render() {
        const { timeZone } = this.props;
        return (
            <Fragment>
                <div className="row">
                    <div className="col">
                        <FormInput id="name" label="Name" value={timeZone.name} onChange={e => this.updateTimeZone(e.target.id, e.target.value)} required />
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <FormInput id="cityName" label="City Name" value={timeZone.cityName} onChange={e => this.updateTimeZone(e.target.id, e.target.value)} required />
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <FormInput id="gmtDifference" type="number" step="1" label="GMT Difference" value={timeZone.gmtDifference} onChange={e => this.updateTimeZone(e.target.id, e.target.value)} required />
                    </div>
                </div>
            </Fragment>
        );
    }
}