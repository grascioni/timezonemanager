import React from 'react';
import { Route, Switch } from 'react-router-dom';
import TimeZoneList from './TimeZoneListPage';
import NewTimeZonePage from './NewTimeZonePage';
import EditTimeZonePage from './EditTimeZonePage';

export default () => (
    <Switch>
        <Route exact path="/timeZones" component={TimeZoneList} />
        <Route exact path="/timeZones/create" component={NewTimeZonePage} />
        <Route exact path="/timeZones/edit/:id?" component={EditTimeZonePage} />
    </Switch>
);
