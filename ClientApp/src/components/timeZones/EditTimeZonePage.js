import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Form } from 'reactstrap';
import TimeZone from './timeZone';
import TimeZoneForm from './TimeZoneForm';
import makeValidable from '../../core/components/Validable';
import Button from '../../core/components/Button';
import { NotificationManager } from 'react-notifications';

const ValidableTimeZoneForm = makeValidable(TimeZoneForm);

class EditTimeZonePage extends Component {
    constructor(props) {
        super(props);
        this.validableRef = React.createRef();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.state = {
            timeZone: null,
            saving: false,
            deleting: false,
            showValidations: false,
            loading: true
        };
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({ showValidations: true }, async () => {
            if (this.validableRef.current.isValid()) {
                this.setState({ saving: true });
                this.state.timeZone.update()
                    .then(() => {
                        NotificationManager.success(`TimeZone saved`, '', 3 * 1000);
                        this.props.history.push('/timeZones');
                    })
                    .finally(() => this.setState({ saving: false }));

            }
        });
    }

    handleDelete() {
        this.setState({ deleting: true });
        this.state.timeZone.delete()
            .then(() => {
                NotificationManager.success(`TimeZone deleted`, '', 3 * 1000);
                this.props.history.push('/timeZones');
            })
            .finally(() => this.setState({ deleting: false }));
    }

    async componentDidMount() {
        const id = this.props.match.params.id;
        const timeZone = await TimeZone.getById(id);
        this.setState({ timeZone, loading: false });
    }

    render() {
        const { timeZone, showValidations, saving, deleting } = this.state;
        if (!timeZone) { return 'Loading...' };
        return (
            <div className="container">
                <Form onSubmit={this.handleSubmit} noValidate={true}>
                    <ValidableTimeZoneForm
                        ref={this.validableRef}
                        showValidations={showValidations}
                        timeZone={timeZone}
                        timeZoneChanged={updated => this.setState({ timeZone: new TimeZone(updated) })} />
                    <div className="row">
                        <div className="col">
                            <Button
                                label="Save"
                                type="submit"
                                className="mt-2 ml-2 btn btn-primary"
                                showSpinner={saving}
                                onClick={this.handleSubmit}
                            />
                            <Button
                                label="Delete"
                                className="mt-2 ml-2 btn btn-danger"
                                showSpinner={deleting}
                                onClick={this.handleDelete}
                            />
                        </div>
                    </div>
                </Form>
            </div>
        );
    }
}

export default withRouter(EditTimeZonePage);
