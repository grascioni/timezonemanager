import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Form } from 'reactstrap';
import TimeZone from './timeZone';
import TimeZoneForm from './TimeZoneForm';
import makeValidable from '../../core/components/Validable';
import Button from '../../core/components/Button';
import { NotificationManager } from 'react-notifications';

const ValidableTimeZoneForm = makeValidable(TimeZoneForm);

class NewTimeZonePage extends Component {
    constructor(props) {
        super(props);
        this.validableRef = React.createRef();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            timeZone: new TimeZone(),
            saving: false,
            showValidations: false,
            loading: true
        };
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({ showValidations: true }, async () => {
            if (this.validableRef.current.isValid()) {
                this.setState({ saving: true });
                TimeZone.create(this.state.timeZone)
                    .then(() => {
                        NotificationManager.success(`TimeZone saved`, '', 3 * 1000);
                        this.props.history.push('/timeZones');
                    })
                    .finally(() => this.setState({ saving: false }));

            }
        });
    }

    render() {
        const { timeZone, showValidations, saving } = this.state;
        return (
            <div className="container">
                <Form onSubmit={this.handleSubmit} noValidate={true}>
                    <ValidableTimeZoneForm
                        ref={this.validableRef}
                        showValidations={showValidations}
                        timeZone={timeZone}
                        timeZoneChanged={updated => this.setState({ timeZone: new TimeZone(updated) })} />
                    <div className="row">
                        <div className="col">
                            <Button
                                label="Save"
                                type="submit"
                                className="mt-2 ml-2 btn btn-primary"
                                showSpinner={saving}
                                onClick={this.handleSubmit}
                            />
                        </div>
                    </div>
                </Form>
            </div>
        );
    }
}

export default withRouter(NewTimeZonePage);
