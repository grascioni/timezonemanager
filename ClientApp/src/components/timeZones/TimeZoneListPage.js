import React, { Component, Fragment } from 'react';
import { withRouter, Link } from 'react-router-dom';
import TimeZone from './timeZone';
import moment from 'moment';
import { formatDateWithTime } from '../../core/util/dateUtilities';
import FormInput from '../../core/components/FormInput';
import BottomScrollListener from '../../core/components/BottomScrollListener';
import pagination from '../../core/fetch/pagination';

class TimeZoneListPage extends Component {
    constructor() {
        super();
        this.handleBottomScroll = this.handleBottomScroll.bind(this);
    }
    state = {
        timeZones: [],
        loading: true,
        nameFilter: '',
        hasLoadedAllPages: false,
        page: 0
    };

    componentDidMount() {
        this.loadPage(0);
    }

    handleBottomScroll() {
        if (!this.state.hasLoadedAllPages) {
            this.loadPage(this.state.page + 1);
        }
    }

    loadPage(page) {
        if (!this.state.loading) {
            this.setState({ loading: true, page });
        }
        TimeZone.search(page)
            .then(items => {
                this.setState({
                    timeZones: page === 0 ? items : this.state.timeZones.concat(items),
                    loading: false,
                    hasLoadedAllPages: items.length < pagination.pageSize
                });
            });
    }

    render() {
        const { timeZones, nameFilter, loading } = this.state;
        const filteredTimeZones = nameFilter !== '' ? timeZones.filter(x => x.name.toLowerCase().includes(nameFilter.toLowerCase())) : timeZones;
        return (
            <div className="container">
                <Link to={`/timeZones/create`}>New TimeZone</Link>
                <hr />
                <FormInput
                    type="text"
                    label="Filter by TZ name"
                    value={nameFilter}
                    onChange={e => this.setState({ nameFilter: e.target.value })}
                />
                <br />
                <BottomScrollListener onBottom={this.handleBottomScroll}>
                    <Fragment>
                        <ul>
                            {filteredTimeZones.map((x, idx) => (
                                <li key={idx} className="mb-4">
                                    <Link to={`/timeZones/edit/${x.timeZoneId}`}>{x.name}</Link>
                                    <br />
                                    <span>Time in {x.cityName}: {formatDateWithTime(moment().utc().add(x.gmtDifference, 'h'))}</span>
                                    <br />
                                    <span>Difference with browser's time (hours): {(new Date().getTimezoneOffset() / 60) + x.gmtDifference}</span>
                                </li>
                            ))}
                        </ul>
                        {loading && <div>Loading...</div>}
                    </Fragment>
                </BottomScrollListener>
            </div>
        );
    }
}

export default withRouter(TimeZoneListPage);
