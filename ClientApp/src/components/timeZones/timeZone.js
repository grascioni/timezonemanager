import apiFetch from '../../core/fetch/apiFetch';
import Filter from '../../core/fetch/filter';

const _baseEndPoint = 'timeZones';

export default class TimeZone {
    constructor(item) {
        if (item) {
            this.timeZoneId = item.timeZoneId;
            this.name = item.name;
            this.cityName = item.cityName;
            this.gmtDifference = item.gmtDifference;
            return;
        }
        this.timeZoneId = null;
        this.name = '';
        this.cityName = '';
        this.gmtDifference = '';
    }

    static async search(page) {
        const payload = new Filter();
        payload.setPaginationParameters(page);
        return apiFetch(`${_baseEndPoint}?${payload.toQueryString()}`);
    }

    static async getById(id) {
        const entity = await apiFetch(`${_baseEndPoint}/${id}`);
        return new TimeZone(entity);
    }

    static create(item) {
        return apiFetch(_baseEndPoint, 'POST', item);
    }

    update() {
        return apiFetch(`${_baseEndPoint}/${this.timeZoneId}`, 'PUT', this);
    }

    delete() {
        return apiFetch(`${_baseEndPoint}/${this.timeZoneId}`, 'DELETE', this);
    }
}