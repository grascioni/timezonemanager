const loginKey = 'login-key';

export const saveLoginData = data => saveGenericData(loginKey, data);

export const saveGenericData = (key, data) =>
    window.localStorage.setItem(key, JSON.stringify(data));

export const getLoginData = () => getGenericData(loginKey);

export const getGenericData = key =>
    JSON.parse(window.localStorage.getItem(key));

export const clearLoginData = () => {
    window.localStorage.removeItem(loginKey);
};
