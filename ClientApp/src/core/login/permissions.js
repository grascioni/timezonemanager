import { getLoginData } from './storage';

function isUserInRole(role) {
    const { userData } = getLoginData();
    return userData && userData.role === role;
}

export function isUserAuthorizedToManageTimeZones() {
    return isUserInRole('Administrator') || isUserInRole('Regular');
}

export function isUserAuthorizedToManageUsers() {
    return isUserInRole('Administrator') || isUserInRole('UserManager');
}
