import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { getLoginData } from '../login/storage';

export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        getLoginData()
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
);