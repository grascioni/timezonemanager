import PropTypes from 'prop-types';
import React from 'react';
import { FormGroup, Label } from 'reactstrap';
import Dropdown from './Dropdown';

const FormDropdown = ({ label, className, ...rest }) => (
    <FormGroup>
        {label && <Label className="form-label">{label}</Label>}
        <Dropdown className={`text-left ${className}`} {...rest} />
    </FormGroup>
);

const sharedPropTypes = {
    label: PropTypes.string.isRequired,
    className: PropTypes.string,
    selectedId: PropTypes.any,
    onChange: PropTypes.func.isRequired
};

FormDropdown.propTypes = {
    ...sharedPropTypes,
    items: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.any.isRequired,
            label: PropTypes.string.isRequired,
            description: PropTypes.string
        })
    ).isRequired
};

export default FormDropdown;
