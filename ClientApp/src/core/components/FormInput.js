import React from 'react';
import PropTypes from 'prop-types';
import Input from './Input';

class FormInput extends React.PureComponent {
    static propTypes = {
        label: PropTypes.string.isRequired,
        formGroupClassName: PropTypes.string,
        id: PropTypes.any
    };

    render() {
        const { id, label, formGroupClassName, ...rest } = this.props;

        return (
            <div className={`"position-relative form-group ${formGroupClassName || ''}`}>
                <label htmlFor={id} className="form-label">
                    {label}
                </label>
                <Input id={id} placeholder={label} {...rest} />
            </div>
        );
    }
}

export default FormInput;
