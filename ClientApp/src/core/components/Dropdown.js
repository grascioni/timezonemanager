import PropTypes from 'prop-types';
import React from 'react';
import Select from 'react-select';
import './Dropdown.scss';
import { ValidableFeedback } from './Validable';

export default class Dropdown extends React.Component {
    static propTypes = {
        items: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.any.isRequired,
                label: PropTypes.string.isRequired
            })
        ).isRequired,
        onChange: PropTypes.func.isRequired,
        selectedId: PropTypes.any,
        className: PropTypes.string,
        required: PropTypes.bool
    };

    static defaultProps = {
        required: false
    };

    render() {
        const {
            className,
            items,
            selectedId,
            onChange,
            readOnly,
            required,
            ...rest
        } = this.props;
        const value = items.find(x => x.id === selectedId) || null;
        const selectClass = `react-select-container ${className}`;
        const invalid = required && !selectedId;

        return (
            <React.Fragment>
                <Select
                    isSearchable={true}
                    classNamePrefix="react-select"
                    className={`${selectClass} ${invalid && 'invalid'} ${required && invalid && 'highlighted'}`}
                    value={value}
                    onChange={e => onChange(e ? e.id : null)}
                    options={items}
                    isDisabled={readOnly}
                    {...rest}
                />
                <ValidableFeedback invalid={invalid} />
            </React.Fragment>
        );
    }
}
