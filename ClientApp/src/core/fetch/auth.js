import { getLoginData } from '../login/storage';

function toFormUrlEncoded(payload) {
    return Object.keys(payload)
        .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(payload[key])}`)
        .join('&');
}

function buildRequest(body) {
    const url = '/api/token';
    const method = 'POST';
    const headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });

    return new Request(url, {
        method,
        headers,
        body
    });
}

function mapValidResponse(json) {
    const { userId: id, fullName: name, role, token } = json;
    return {
        userData: {
            id,
            name,
            role
        },
        token
    };
}

function mapBadRequestResponse(json) {
    return json.message;
}

async function register(userData) {
    const url = '/api/token/register';
    const method = 'POST';
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    const body = JSON.stringify(userData);
    const request = new Request(url, {
        method,
        headers,
        body
    });
    await fetch(request);
}

async function login(credential) {
    const body = toFormUrlEncoded({
        grant_type: 'password',
        username: credential.userName,
        password: credential.password
    });
    const response = await fetch(buildRequest(body));
    const json = await response.json();
    if (response.ok) return mapValidResponse(json);
    if (response.status === 400) throw mapBadRequestResponse(json);
    throw Error(response);
}

function isPasswordValid(str) {
    return str.length >= 8;
}

function getAuthHeaders() {
    return new Promise((resolve, reject) => {
        setTimeout(function () {
            const loginData = getLoginData();
            !!(loginData)
                ? resolve({
                    Authorization: `Bearer ${loginData.token}`
                })
                : reject('Error');
        }, 2000);
    });
}

function navigateToLogout() {
    document.location = '/logout';
}

export { login, isPasswordValid, getAuthHeaders, navigateToLogout, register };
