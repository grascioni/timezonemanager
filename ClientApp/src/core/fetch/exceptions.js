export class HttpUnexpectedResponseError {
    constructor(httpResponse) {
        this.httpResponse = httpResponse;
    }
}

export class HttpUnauthorizedResponseError { }
