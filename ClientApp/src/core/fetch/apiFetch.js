import processHttpResponse from './processHttpResponse';
import { getLoginData } from '../login/storage';
import { navigateToLogout } from './auth';

function completetMissingOptions(options) {
    return Object.assign(
        {
            contentType: 'application/json',
            stringifyContent: true,
            automaticSuccessNotification: true
        },
        options
    );
}

function apiFetch(url, method = 'GET', payload, options) {
    options = completetMissingOptions(options);

    const loginData = getLoginData();
    if (!loginData) {
        navigateToLogout();
        return Promise.reject('User must log in');
    }

    const headers = (function () {
        const headers = new Headers();
        headers.append('Authorization', `Bearer ${loginData.token}`);
        if (options.contentType) {
            headers.append('Content-Type', options.contentType);
        }
        return headers;
    })();

    const body = (function () {
        if (!payload) return null;
        if (options.stringifyContent) return JSON.stringify(payload);
        return payload;
    })();

    const request = new Request(`/api/${url}`, {
        method,
        headers,
        body
    });

    const processHttpResponseOptions = {
        automaticSuccessNotification: options.automaticSuccessNotification
    };

    return fetch(request)
        .then(response => processHttpResponse(response, processHttpResponseOptions))
        .catch(async e => {
            return Promise.reject(e);
        });
}

export default apiFetch;
