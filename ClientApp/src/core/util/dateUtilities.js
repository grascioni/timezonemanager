import moment from 'moment';

export const formatDate = date => moment(date).format('L');
export const formatDateLongMonth = date => moment(date).format('MMMM DD, YYYY');
export const formatDateShortMonth = date => moment(date).format('MMM DD, YYYY');
export const formatDateWithTime = date => moment(date).format('LLL');
