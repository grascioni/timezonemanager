export const newLineString = String.fromCharCode(10);

export function howManyLinesIn(string) {
    return ('' + string).split(newLineString).length;
}
