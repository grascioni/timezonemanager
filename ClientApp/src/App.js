import React, { Component, Fragment } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { NotificationContainer } from 'react-notifications';
import { PrivateRoute } from './core/components/PrivateRoute';
import NotFound from './core/components/NotFound';
import Login from './components/login/LoginPage';
import LogoutPage from './components/login/LogoutPage';
import RegisterPage from './components/login/RegisterPage';
import UserRoutes from './components/users';
import TimeZoneRoutes from './components/timeZones';
import { NavMenu } from './components/NavMenu';
import { Home } from './components/Home';

const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href');

function noMenuRoute(pathname) {
    const routes = ['/login', '/register'];
    return routes.includes(pathname);
}

export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <BrowserRouter basename={baseUrl}>
                <Fragment>
                    <NotificationContainer />
                    <Route path="/" render={routeProps => (noMenuRoute(routeProps.location.pathname) ? null : <NavMenu />)} />
                    <Switch>
                        <PrivateRoute exact path='/' component={Home} />
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/register" component={RegisterPage} />
                        <PrivateRoute exact path="/logout" component={LogoutPage} />
                        <PrivateRoute path="/users" component={UserRoutes} />
                        <PrivateRoute path="/timeZones" component={TimeZoneRoutes} />
                        <PrivateRoute component={NotFound} />
                    </Switch>
                </Fragment>
            </BrowserRouter>
        );
    }
}
