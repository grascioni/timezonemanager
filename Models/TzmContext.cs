﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Linq;
using TimeZoneManager.Infrastructure;

namespace TimeZoneManager.Models
{
    public class TzmContext : DbContext
    {
        public TzmContext(DbContextOptions<TzmContext> options)
        : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var nullStringConversion = new ValueConverter<string, string>(v => v == "" ? null : v, v => v);

            var administratorUser = User.GetAdministratorUser();
            modelBuilder.Entity<User>().HasData(administratorUser);
            modelBuilder.Entity<User>().HasOne(c => c.UserRole).WithMany().HasForeignKey(c => c.UserRoleId);

            modelBuilder.Entity<UserRole>().HasData(
                Enum.GetValues(typeof(UserRoleEnum))
                .OfType<UserRoleEnum>()
                .Select(x => new UserRole { UserRoleId = (int)x, Description = x.GetDescription() })
                .ToArray()
            );
        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<TimeZone> TimeZones { get; set; }
    }
}