using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TimeZoneManager.Models
{
    public enum UserRoleEnum
    {
        [Description("Regular")]
        Regular = 1,
        [Description("User Manager")]
        UserManager = 2,
        [Description("Administrator")]
        Administrator = 3
    }

    public class UserRole
    {
        public int UserRoleId { get; set; }

        [Required]
        public string Description { get; set; }
    }
}