﻿using System.ComponentModel.DataAnnotations;

namespace TimeZoneManager.Models
{
    public class TimeZone
    {
        public int TimeZoneId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string CityName { get; set; }

        public int GmtDifference { get; set; }

        public int UserId { get; set; }
    }
}
