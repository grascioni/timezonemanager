﻿using TimeZoneManager.Infrastructure;
using System.ComponentModel.DataAnnotations;

namespace TimeZoneManager.Models
{
    public class User
    {
        public int UserId { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public byte[] PasswordHash { get; set; }

        [Required]
        public byte[] PasswordSalt { get; set; }

        public int UserRoleId { get; set; }

        public UserRole UserRole { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public static User GetAdministratorUser()
        {
            PasswordHasher.CreatePasswordHash("Password1!", out byte[] passwordHash, out byte[] passwordSalt);

            return new User()
            {
                UserId = 1,
                FirstName = "John",
                LastName = "Doe",
                Username = "admin",
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                UserRoleId = (int)UserRoleEnum.Administrator
            };
        }
    }
}
