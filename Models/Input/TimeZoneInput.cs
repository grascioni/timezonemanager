﻿namespace TimeZoneManager.Models.Input
{
    public class TimeZoneInput
    {
        public string Name { get; set; }
        public string CityName { get; set; }
        public int GmtDifference { get; set; }
    }
}
