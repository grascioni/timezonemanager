using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using TimeZoneManager.Infrastructure;
using TimeZoneManager.Models;
using TimeZoneManager.Models.Input;
using TimeZoneManager.Services;

namespace TimeZoneManager.Controllers
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class TokenController : Controller
    {
        private readonly IConfiguration Configuration;
        private readonly JwtOptions JwtOptions;
        private readonly IUserService UserService;
        private readonly TzmContext Db;

        public TokenController(IConfiguration configuration, JwtOptions jwtOptions, IUserService userService, TzmContext tzmContext)
        {
            Configuration = configuration;
            JwtOptions = jwtOptions;
            UserService = userService;
            Db = tzmContext;
        }

        [HttpPost("")]
        public IActionResult Index([FromForm] LoginModel model)
        {
            var user = UserService.Authenticate(model.Username, model.Password);
            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            var claims = new List<Claim> {
                new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString()),
                new Claim(ClaimTypes.Role, ((UserRoleEnum)user.UserRoleId).ToString())
            };

            var token = new JwtSecurityToken(
                issuer: JwtOptions.Issuer,
                audience: JwtOptions.Audience,
                claims: claims,
                expires: DateTime.Now.AddHours(24),
                signingCredentials: JwtOptions.SigningCredentials);

            return Ok(new
            {
                user.UserId,
                user.FullName,
                token = new JwtSecurityTokenHandler().WriteToken(token),
                role = ((UserRoleEnum)user.UserRoleId).ToString()
            });
        }

        [HttpPost("register")]
        public void Register([FromBody] UserInput userInput)
        {
            //TODO: Validate username uniqueness.

            PasswordHasher.CreatePasswordHash(userInput.Password, out byte[] passwordHash, out byte[] passwordSalt);

            var user = new User()
            {
                FirstName = userInput.FirstName,
                LastName = userInput.LastName,
                Username = userInput.Username,
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                UserRoleId = (int)UserRoleEnum.Regular
            };
            Db.Users.Add(user);
            Db.SaveChanges();
        }
    }
}