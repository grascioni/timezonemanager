using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using TimeZoneManager.Infrastructure;
using TimeZoneManager.Infrastructure.Authorization;
using TimeZoneManager.Models;
using TimeZoneManager.Models.Input;

namespace TimeZoneManager.Controllers
{
    [Route("api/[controller]")]
    [AuthorizeRole(UserRoleEnum.Administrator, UserRoleEnum.Regular)]
    public class TimeZonesController : Controller
    {
        private readonly TzmContext Db;

        private int UserId
        {
            get
            {
                return int.Parse(User.Claims.Single(x => x.Type == ClaimTypes.NameIdentifier).Value);
            }
        }

        public TimeZonesController(TzmContext tzmContext)
        {
            Db = tzmContext;
        }

        [HttpGet("{timeZoneId}")]
        public TimeZone GetById(int timeZoneId)
        {
            var timeZone = Db.TimeZones.Single(x => x.TimeZoneId == timeZoneId);
            if (!User.IsInRole("Administrator") && timeZone.UserId != UserId)
                throw new NotAuthorizedException("You don't have access to that resource");
            return timeZone;
        }

        [HttpGet("")]
        [EnableQuery()]
        public IEnumerable<TimeZone> Search()
        {
            var isUserAdministrator = User.IsInRole(nameof(UserRoleEnum.Administrator));
            return Db.TimeZones.Where(x => x.UserId == UserId || isUserAdministrator);
        }

        [HttpPost("")]
        public void Create([FromBody] TimeZoneInput timeZoneInput)
        {
            var timeZone = new TimeZone()
            {
                Name = timeZoneInput.Name,
                CityName = timeZoneInput.CityName,
                GmtDifference = timeZoneInput.GmtDifference,
                UserId = UserId
            };
            Db.TimeZones.Add(timeZone);
            Db.SaveChanges();
        }

        [HttpPut("{timeZoneId}")]
        public void Update(int timeZoneId, [FromBody] TimeZoneInput timeZoneInput)
        {
            var timeZone = Db.TimeZones.Single(x => x.TimeZoneId == timeZoneId);

            timeZone.Name = timeZoneInput.Name;
            timeZone.CityName = timeZoneInput.CityName;
            timeZone.GmtDifference = timeZoneInput.GmtDifference;

            if (!User.IsInRole("Administrator") && timeZone.UserId != UserId)
                throw new NotAuthorizedException("You don't have access to that resource");

            Db.SaveChanges();
        }

        [HttpDelete("{timeZoneId}")]
        public void Delete(int timeZoneId)
        {
            var timeZone = Db.TimeZones.Single(x => x.TimeZoneId == timeZoneId);

            if (!User.IsInRole("Administrator") && timeZone.UserId != UserId)
                throw new NotAuthorizedException("You don't have access to that resource");

            Db.TimeZones.Remove(timeZone);
            Db.SaveChanges();
        }
    }
}
