using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using TimeZoneManager.Infrastructure.Authorization;
using TimeZoneManager.Models;

namespace TimeZoneManager.Controllers
{
    [Route("api/[controller]")]
    [AuthorizeRole(UserRoleEnum.Administrator, UserRoleEnum.UserManager)]
    public class UserRolesController : Controller
    {
        private readonly TzmContext Db;

        public UserRolesController(TzmContext tzmContext)
        {
            Db = tzmContext;
        }

        [HttpGet("")]
        public IEnumerable<UserRole> Search()
        {
            return Db.UserRoles;
        }
    }
}
