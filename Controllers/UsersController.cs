using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using TimeZoneManager.Infrastructure;
using TimeZoneManager.Infrastructure.Authorization;
using TimeZoneManager.Models;
using TimeZoneManager.Models.Input;

namespace TimeZoneManager.Controllers
{
    [Route("api/[controller]")]
    [AuthorizeRole(UserRoleEnum.Administrator, UserRoleEnum.UserManager)]
    public class UsersController : Controller
    {
        private readonly TzmContext Db;

        public UsersController(TzmContext tzmContext)
        {
            Db = tzmContext;
        }

        [HttpGet("{userId}")]
        public User GetById(int userId)
        {
            return Db.Users.Single(x => x.UserId == userId);
        }

        [HttpGet("")]
        public IEnumerable<User> Search()
        {
            return Db.Users;
        }

        [HttpPost("")]
        public void Create([FromBody] UserInput userInput)
        {
            PasswordHasher.CreatePasswordHash(userInput.Password, out byte[] passwordHash, out byte[] passwordSalt);

            var user = new User()
            {
                FirstName = userInput.FirstName,
                LastName = userInput.LastName,
                Username = userInput.Username,
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                UserRoleId = userInput.UserRoleId.Value
            };
            Db.Users.Add(user);
            Db.SaveChanges();
        }

        [HttpPut("{userId}")]
        public void Update(int userId, [FromBody] UserInput userInput)
        {
            PasswordHasher.CreatePasswordHash(userInput.Password, out byte[] passwordHash, out byte[] passwordSalt);

            var user = Db.Users.Single(x => x.UserId == userId);

            user.FirstName = userInput.FirstName;
            user.LastName = userInput.LastName;
            user.Username = userInput.Username;
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            if (User.IsInRole("Administrator"))
                user.UserRoleId = userInput.UserRoleId.Value;

            Db.SaveChanges();
        }

        [HttpDelete("{userId}")]
        public void Delete(int userId)
        {
            var user = Db.Users.Single(x => x.UserId == userId);

            Db.Users.Remove(user);
            Db.SaveChanges();
        }
    }
}
