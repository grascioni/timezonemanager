﻿using System;
using System.Collections.Generic;
using System.Linq;
using TimeZoneManager.Infrastructure;
using TimeZoneManager.Models;

namespace TimeZoneManager.Services
{
    public class UserService : IUserService
    {
        private readonly TzmContext Db;

        public UserService(TzmContext posContext)
        {
            Db = posContext;
        }

        public User Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var user = Db.Users.SingleOrDefault(x => x.Username == username);

            if (user == null)
                return null;

            if (!PasswordHasher.VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            return user;
        }

        public IEnumerable<User> GetAll()
        {
            return Db.Users;
        }

        public User GetById(int id)
        {
            return Db.Users.Find(id);
        }

        public User Create(User user, string password)
        {
            if (string.IsNullOrWhiteSpace(password))
                throw new Exception("Password is required");

            if (Db.Users.Any(x => x.Username == user.Username))
                throw new Exception($"Username {user.Username} is already taken");

            PasswordHasher.CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            Db.Users.Add(user);
            Db.SaveChanges();

            return user;
        }

        public void Update(User userParam, string password = null)
        {
            var user = Db.Users.Find(userParam.UserId);

            if (user == null)
                throw new Exception("User not found");

            if (userParam.Username != user.Username)
            {
                if (Db.Users.Any(x => x.Username == userParam.Username))
                    throw new Exception($"Username {userParam.Username} is already taken");
            }

            user.FirstName = userParam.FirstName;
            user.LastName = userParam.LastName;
            user.Username = userParam.Username;

            if (!string.IsNullOrWhiteSpace(password))
            {
                PasswordHasher.CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
            }

            Db.Users.Update(user);
            Db.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = Db.Users.Find(id);
            if (user != null)
            {
                Db.Users.Remove(user);
                Db.SaveChanges();
            }
        }
    }
}
