﻿using Microsoft.AspNetCore.Authorization;
using System;
using TimeZoneManager.Models;

namespace TimeZoneManager.Infrastructure.Authorization
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class AuthorizeRoleAttribute : AuthorizeAttribute
    {
        public AuthorizeRoleAttribute(params UserRoleEnum[] roles)
        {
            Roles = string.Join(",", roles);
        }
    }
}
