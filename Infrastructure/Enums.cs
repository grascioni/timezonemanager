﻿using System;
using System.ComponentModel;

namespace TimeZoneManager.Infrastructure
{
    public static class Enums
    {
        public static string GetDescription(this Enum value)
        {
            var field = value.GetType().GetField(value.ToString());

            return Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) is DescriptionAttribute attribute ?
                attribute.Description : value.ToString();
        }
    }
}