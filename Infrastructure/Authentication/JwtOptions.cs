﻿using Microsoft.IdentityModel.Tokens;

namespace TimeZoneManager.Infrastructure
{
    public class JwtOptions
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public SigningCredentials SigningCredentials { get; set; }
    }
}
