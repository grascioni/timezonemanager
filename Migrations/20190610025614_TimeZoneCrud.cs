﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TimeZoneManager.Migrations
{
    public partial class TimeZoneCrud : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TimeZones",
                columns: table => new
                {
                    TimeZoneId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    CityName = table.Column<string>(nullable: false),
                    GmtDifference = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimeZones", x => x.TimeZoneId);
                });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 1,
                columns: new[] { "PasswordHash", "PasswordSalt" },
                values: new object[] { new byte[] { 124, 152, 208, 134, 162, 22, 71, 49, 102, 99, 116, 107, 20, 248, 120, 183, 254, 124, 77, 151, 85, 173, 57, 7, 106, 5, 224, 158, 132, 77, 212, 184, 250, 183, 115, 234, 138, 225, 29, 155, 250, 160, 54, 38, 38, 154, 214, 87, 64, 185, 20, 74, 72, 153, 76, 188, 231, 186, 9, 201, 145, 153, 85, 208 }, new byte[] { 186, 177, 50, 83, 123, 8, 173, 47, 250, 245, 218, 193, 227, 107, 146, 212, 39, 220, 63, 225, 66, 114, 118, 194, 158, 72, 20, 249, 176, 160, 181, 21, 253, 50, 93, 230, 43, 203, 214, 242, 164, 182, 194, 95, 38, 120, 97, 180, 115, 109, 71, 91, 186, 216, 200, 21, 151, 92, 198, 168, 180, 90, 241, 230, 59, 131, 75, 236, 0, 61, 20, 254, 67, 97, 54, 75, 3, 156, 17, 205, 251, 20, 183, 147, 18, 172, 218, 142, 149, 88, 113, 11, 114, 82, 22, 87, 226, 133, 68, 211, 126, 192, 175, 15, 182, 102, 15, 212, 201, 63, 128, 175, 195, 229, 57, 22, 199, 167, 3, 192, 201, 214, 13, 179, 74, 15, 33, 110 } });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TimeZones");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 1,
                columns: new[] { "PasswordHash", "PasswordSalt" },
                values: new object[] { new byte[] { 145, 43, 173, 12, 135, 88, 99, 93, 254, 47, 146, 252, 89, 20, 137, 110, 132, 80, 111, 147, 176, 188, 175, 158, 181, 80, 212, 235, 9, 127, 73, 221, 211, 61, 66, 202, 160, 175, 217, 43, 15, 236, 37, 29, 234, 185, 66, 181, 243, 174, 15, 120, 8, 242, 75, 170, 230, 240, 35, 150, 163, 5, 224, 255 }, new byte[] { 72, 29, 178, 220, 20, 32, 13, 125, 82, 12, 123, 119, 195, 165, 11, 164, 190, 121, 8, 15, 33, 207, 152, 69, 6, 63, 172, 22, 119, 9, 0, 56, 177, 146, 216, 242, 74, 21, 65, 205, 63, 18, 134, 103, 196, 183, 179, 194, 210, 23, 81, 8, 226, 112, 62, 72, 190, 115, 252, 47, 37, 175, 197, 93, 34, 51, 131, 188, 155, 111, 36, 170, 137, 245, 220, 49, 188, 152, 140, 114, 229, 231, 131, 123, 62, 171, 166, 60, 88, 235, 26, 45, 44, 8, 41, 152, 217, 185, 90, 202, 33, 169, 197, 7, 60, 127, 88, 198, 209, 89, 240, 210, 170, 63, 79, 30, 161, 72, 53, 204, 38, 149, 16, 118, 128, 3, 27, 25 } });
        }
    }
}
