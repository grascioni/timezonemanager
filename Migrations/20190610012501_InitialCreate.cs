﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TimeZoneManager.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    UserRoleId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => x.UserRoleId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    Username = table.Column<string>(nullable: false),
                    PasswordHash = table.Column<byte[]>(nullable: false),
                    PasswordSalt = table.Column<byte[]>(nullable: false),
                    UserRoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_Users_UserRoles_UserRoleId",
                        column: x => x.UserRoleId,
                        principalTable: "UserRoles",
                        principalColumn: "UserRoleId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "UserRoles",
                columns: new[] { "UserRoleId", "Description" },
                values: new object[] { 1, "Regular" });

            migrationBuilder.InsertData(
                table: "UserRoles",
                columns: new[] { "UserRoleId", "Description" },
                values: new object[] { 2, "User Manager" });

            migrationBuilder.InsertData(
                table: "UserRoles",
                columns: new[] { "UserRoleId", "Description" },
                values: new object[] { 3, "Administrator" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "FirstName", "LastName", "PasswordHash", "PasswordSalt", "UserRoleId", "Username" },
                values: new object[] { 1, "John", "Doe", new byte[] { 145, 43, 173, 12, 135, 88, 99, 93, 254, 47, 146, 252, 89, 20, 137, 110, 132, 80, 111, 147, 176, 188, 175, 158, 181, 80, 212, 235, 9, 127, 73, 221, 211, 61, 66, 202, 160, 175, 217, 43, 15, 236, 37, 29, 234, 185, 66, 181, 243, 174, 15, 120, 8, 242, 75, 170, 230, 240, 35, 150, 163, 5, 224, 255 }, new byte[] { 72, 29, 178, 220, 20, 32, 13, 125, 82, 12, 123, 119, 195, 165, 11, 164, 190, 121, 8, 15, 33, 207, 152, 69, 6, 63, 172, 22, 119, 9, 0, 56, 177, 146, 216, 242, 74, 21, 65, 205, 63, 18, 134, 103, 196, 183, 179, 194, 210, 23, 81, 8, 226, 112, 62, 72, 190, 115, 252, 47, 37, 175, 197, 93, 34, 51, 131, 188, 155, 111, 36, 170, 137, 245, 220, 49, 188, 152, 140, 114, 229, 231, 131, 123, 62, 171, 166, 60, 88, 235, 26, 45, 44, 8, 41, 152, 217, 185, 90, 202, 33, 169, 197, 7, 60, 127, 88, 198, 209, 89, 240, 210, 170, 63, 79, 30, 161, 72, 53, 204, 38, 149, 16, 118, 128, 3, 27, 25 }, 3, "admin" });

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserRoleId",
                table: "Users",
                column: "UserRoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "UserRoles");
        }
    }
}
